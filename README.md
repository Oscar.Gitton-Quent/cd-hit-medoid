# cd-hit-medoid
**cd-hit-medoid:** Algorithm to correct the output of the cd-hit-est sequence clustering alogorithm by detection/fragmentation of clusters build with chimera sequences and selection of clusters representatives. Takes the .clstr output of the cd-hit-est algorithm, the file of the gene sequences to be clustered and the path to the files of the contigs/genomes from which the genes are derived. Returns the files of the sequences of genes representative of the clusters as well as the descriptive sheet of these genes and this before and after elimination of the final redundancy (.cor). Required to run the external tools parallel (build 02.20.2016), blastn (v 2.7.1), seqkit (v 0.8.1).
Parameters:
-   -fas: path to the files of the nucleotide sequences of the contigs/genomes
-   -fna: gene sequence file before cluterisation by cd-hit-est
-   -c: .clstr output file of the gene clustering via cd-hit-est
-   -s: minimum gene size allowed for the detection of chimeric clusters (recommended size: 1000)
-   -ws: word size used for alignment via blastn (recommended size: 20)
-   -tmp : path to the temporary data storage folder
-   -o: prefix of the output file
-   -t: number of threads

Exemple:
cd-hit-medoid –fas /way/to/contigs/file/* –fna /file/of/genes.fna –c /file/of/clusters/by/cd-hit-est/output.clstr –s 1000 –ws 20 –tmp /way/to/temporary/files/ –o /output/file –t 10  
Output 
-	/output/file		-> File of genes selection before redundancy curation
-	/output/file.stat	-> File of genes information before redundancy curation
-	/output/file.cor		-> File of genes selection after redundancy curation
-	/output/file.cor.stat	-> File of genes information after redundancy curation



**iteratif-medoide:** Algorithm allowing to concatenate 2 gene files by eliminating redundancy in the image of cd-hit-2d. Selection of the gene represented based on the file origin of the gene, the number of genes in the gene cluster and the size of the gene. Takes in 2 non-redundant gene files and associated .stat files generated via cd-hit-medoid. Returns the non-redundant gene concatenation file and the associated .stat file. Required to run external tools parallel (build 02.20.2016), blastn (v 2.7.1), seqkit (v 0.8.1).
Parameters
-   -in1: priority non-redundant gene file from cd-hit-medoid  
-   -in2: secondary non-redundant gene file from cd-hit-medoid
-   -stat1: priority non-redundant gene file.stat from cd-hit-medoid
-   -stat2: secondary non-redundant gene file.stat from cd-hit-medoid
-   -tmp : path to the temporary data storage folder
-   -tseuil: minimum gene size allowed for selection of replacement genes (recommended size: 1000)
-   -ws: word size used for alignment via blastn (recommended size: 20)
-   -out: Prefix of the output file
-   -tread: Number of threads

Exemple :
iteratif-medoide –in1 /file/clust1.fna –in2 /file/clust2.fna –stat1 /file/clust1.fna.stat –stat2 /file/clust2.fna.stat –tmp /way/to/temporary/files/ -tseuil 1000 –ws 20 –out /output/file –tread 10
Output:
-	/output/file		-> File of the concatenation of genes sequences from file1 and file2
-	/output/file.stat	-> File of the concatenation of genes information from file1 and file2

